package cl.it.commons.http;

import java.util.List;

import cl.it.commons.error.SimpleError;

public class Response<T> {

	private Status status;
	private T data;
	private String message;
	private List<SimpleError> errors;
	
	public Response() {}
	
	public Response(T data) {
		this.setStatus(Status.SUCCESS);
		this.setData(data);
	}
	
	public Response(Status status, String message) {
		this.setStatus(status);
		this.setMessage(message);
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public List<SimpleError> getErrors() {
		return errors;
	}

	public void setErrors(List<SimpleError> errors) {
		this.errors = errors;
	}

	@Override
	public String toString() {
		return "Response [status=" + status + ", data=" + data + ", message=" + message + ", errors=" + errors + "]";
	}

}
