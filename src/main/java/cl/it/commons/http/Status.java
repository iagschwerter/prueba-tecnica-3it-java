package cl.it.commons.http;

public enum Status {

	SUCCESS,
	PARTIAL_SUCCESS,
	FAIL,
	ERROR,
	NOT_FOUND;
}
