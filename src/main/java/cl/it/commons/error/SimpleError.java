package cl.it.commons.error;

import cl.it.commons.http.Status;

public class SimpleError {
	private Status status;
	private String message;
	
	public SimpleError() {
		
	}
	
	public SimpleError(Status status, String message) {
		this.status = status;
		this.message = message;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return "SimpleError [status=" + status + ", message=" + message + "]";
	}
	
	
}
