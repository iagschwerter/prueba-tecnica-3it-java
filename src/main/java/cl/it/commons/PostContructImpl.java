package cl.it.commons;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Component;

import cl.it.adapter.bd.prueba3it.entity.GeneroMusicalEntity;
import cl.it.adapter.bd.prueba3it.repository.EncuestaMusicaRepository;
import cl.it.adapter.bd.prueba3it.repository.GeneroMusicalRepository;
import lombok.RequiredArgsConstructor;

@Component
@RequiredArgsConstructor
public class PostContructImpl {
	
	private final GeneroMusicalRepository repo;

	@PostConstruct
	public void runAfterObjectCreated() {
		
		List<GeneroMusicalEntity> listageneros= new ArrayList<GeneroMusicalEntity>();
		GeneroMusicalEntity genero1= new GeneroMusicalEntity();
		genero1.setNombreGenero("Rock");
		genero1.setIdGenero(1);
		listageneros.add(genero1);
		GeneroMusicalEntity genero2= new GeneroMusicalEntity();
		genero2.setNombreGenero("Pop");
		genero2.setIdGenero(2);
		listageneros.add(genero2);
		GeneroMusicalEntity genero3= new GeneroMusicalEntity();
		genero3.setNombreGenero("JAZZ");
		genero3.setIdGenero(3);
		listageneros.add(genero3);
		GeneroMusicalEntity genero4= new GeneroMusicalEntity();
		genero4.setNombreGenero("CLASICA");
		genero4.setIdGenero(4);
		listageneros.add(genero4);
		GeneroMusicalEntity genero5= new GeneroMusicalEntity();
		genero5.setNombreGenero("Reggeaton");
		genero5.setIdGenero(5);
		listageneros.add(genero5);
		repo.saveAll(listageneros);
		
		List<GeneroMusicalEntity> repore=repo.findAll();
		System.out.println(repore);
	
	}

}
