package cl.it.adapter.bd.prueba3it.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import cl.it.adapter.bd.prueba3it.entity.GeneroMusicalEntity;


public interface GeneroMusicalRepository  extends JpaRepository<GeneroMusicalEntity, Integer> {


}
