package cl.it.adapter.bd.prueba3it.service;

import java.util.List;

import org.springframework.stereotype.Service;

import cl.it.adapter.bd.prueba3it.entity.GeneroMusicalEntity;
import cl.it.adapter.bd.prueba3it.repository.GeneroMusicalRepository;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class GeneroMusicalService {
	
	
	private final GeneroMusicalRepository generoMusicalRepository;
	
	public List<GeneroMusicalEntity> getAll() {
	 return	generoMusicalRepository.findAll();
	}
	
	public GeneroMusicalEntity findbyId(int id) {
		return generoMusicalRepository.findById(id).get();
	}

}
