package cl.it.adapter.bd.prueba3it.configuration;

import java.util.HashMap;

import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import cl.it.properties.BdPrueba3ItProperties;
import lombok.RequiredArgsConstructor;

@Configuration
@RequiredArgsConstructor
@EnableTransactionManagement
@EnableJpaRepositories(
		basePackages = "cl.it.adapter.bd.prueba3it.repository",
		entityManagerFactoryRef = "prueba3itEntityManager",
		transactionManagerRef= "prueba3itTransactionManager"
)
public class DatabasePrueba3ItConfiguration {
	
	private final BdPrueba3ItProperties bdProperties;
	 @Bean
	    public LocalContainerEntityManagerFactoryBean prueba3itEntityManager() {
	    	
	    	LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
			em.setDataSource(prueba3itDataSource());
			em.setPackagesToScan("cl.it.adapter.bd.prueba3it.entity");

			HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
			em.setJpaVendorAdapter(vendorAdapter);
	      
			HashMap<String, Object> properties = new HashMap<>();
			properties.put("hibernate.hbm2ddl.auto", bdProperties.getHibernateHbm2ddlAuto());
			properties.put("hibernate.dialect", bdProperties.getHibernateDialect());
			em.setJpaPropertyMap(properties);

			return em;
	    }
	    
	    @Bean
	    public DataSource prueba3itDataSource() {
	    	
	    	DriverManagerDataSource dataSource = new DriverManagerDataSource();
	        dataSource.setDriverClassName(bdProperties.getDriverClassName());
	        dataSource.setUrl(bdProperties.getUrl());
	        dataSource.setUsername(bdProperties.getUsername());
	        dataSource.setPassword(bdProperties.getPassword());
	 
	        return dataSource;
	    }
	    
	    @Bean
	    public PlatformTransactionManager prueba3itTransactionManager() {
	    	JpaTransactionManager transactionManager = new JpaTransactionManager();
	        transactionManager.setEntityManagerFactory(prueba3itEntityManager().getObject());
	        
	        return transactionManager;
	    }
}
