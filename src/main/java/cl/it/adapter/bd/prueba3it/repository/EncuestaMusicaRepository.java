package cl.it.adapter.bd.prueba3it.repository;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;


import cl.it.adapter.bd.prueba3it.entity.EncuestaMusicaEntity;


public interface EncuestaMusicaRepository extends PagingAndSortingRepository<EncuestaMusicaEntity, String>, JpaSpecificationExecutor<EncuestaMusicaEntity> 
{	
	Page<EncuestaMusicaEntity> findAll(Pageable pageable);

}
