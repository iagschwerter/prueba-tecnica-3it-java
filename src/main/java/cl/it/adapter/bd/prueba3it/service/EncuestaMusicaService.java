package cl.it.adapter.bd.prueba3it.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import cl.it.adapter.bd.prueba3it.entity.EncuestaMusicaEntity;
import cl.it.adapter.bd.prueba3it.repository.EncuestaMusicaRepository;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class EncuestaMusicaService {
	
	private final EncuestaMusicaRepository encuestaMusicaRepository;
	
	
	public void create(EncuestaMusicaEntity entity) {
		encuestaMusicaRepository.save(entity);

	}
	
	public Boolean existe(String correo) {
		

		return encuestaMusicaRepository.findById(correo).isPresent();

	}
	public Page<EncuestaMusicaEntity> getAll(Pageable pageable ) {
		return encuestaMusicaRepository.findAll(pageable);

	}
}
