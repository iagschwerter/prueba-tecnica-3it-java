package cl.it.adapter.bd.prueba3it.entity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@Entity
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
@Table(name = "encuesta_musica")
public class EncuestaMusicaEntity {

	
	@Id
	private String mail;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "genero_musical_id")
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	private GeneroMusicalEntity generoMusical;
	
	public EncuestaMusicaEntity() {
		super();
	}
	public EncuestaMusicaEntity(EncuestaMusicaEntity entity) {
		super();
		this.mail=entity.getMail();
		this.generoMusical=entity.getGeneroMusical();
	}
	
}
