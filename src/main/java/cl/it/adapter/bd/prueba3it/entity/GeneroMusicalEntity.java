package cl.it.adapter.bd.prueba3it.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


import lombok.Data;

@Data
@Entity
@Table(name = "genero_musical")
public class GeneroMusicalEntity {
	
	@Id
	@Column(name="id_genero_musical")
	private int idGenero;
	@Column(name="nombre_genero_musical")
	private String nombreGenero; 

}
