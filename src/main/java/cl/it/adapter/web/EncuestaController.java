package cl.it.adapter.web;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cl.it.adapter.bd.prueba3it.entity.EncuestaMusicaEntity;
import cl.it.adapter.bd.prueba3it.service.EncuestaMusicaService;
import cl.it.application.GuardaEncuestaService;
import cl.it.commons.http.Response;
import cl.it.domain.EncuestaDto;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/v1/encuesta")
@RequiredArgsConstructor
@CrossOrigin(origins = { "http://localhost:4200" })
public class EncuestaController {

	private final GuardaEncuestaService guardaEncuestaService;
	private final EncuestaMusicaService encuestaMusicaService;
	@PostMapping("/crear")
	public Response<Void> crear(@RequestBody EncuestaDto dto ) throws Exception {
		

		return guardaEncuestaService.guardaEncuestaMusical(dto);
	}
	
	@GetMapping("/getall")
	public Page<EncuestaMusicaEntity> getListaEncuesta(Pageable pageable) {
		return encuestaMusicaService.getAll(pageable);
		

	}
}
