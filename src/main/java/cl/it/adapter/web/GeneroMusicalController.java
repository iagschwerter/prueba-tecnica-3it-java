package cl.it.adapter.web;

import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cl.it.adapter.bd.prueba3it.entity.GeneroMusicalEntity;
import cl.it.adapter.bd.prueba3it.service.GeneroMusicalService;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/v1/genero")
@RequiredArgsConstructor
@CrossOrigin(origins = { "http://localhost:4200" })
public class GeneroMusicalController {
	
	private final GeneroMusicalService generoMusicalService;
	
	
	@GetMapping("/getall")
	public List<GeneroMusicalEntity> getAll() {
		return generoMusicalService.getAll();
	}
}
