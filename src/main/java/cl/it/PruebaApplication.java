package cl.it;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import cl.it.properties.BdPrueba3ItProperties;



@SpringBootApplication
@EnableConfigurationProperties
@EntityScan(basePackages = {"cl.it.adapter.bd.prueba3it.entity"})
@ConfigurationPropertiesScan(basePackageClasses = {
		BdPrueba3ItProperties.class})
public class PruebaApplication {

	public static void main(String[] args) {
		SpringApplication.run(PruebaApplication.class, args);
	}

}
