package cl.it.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Data;

@Data
//@ConfigurationProperties(prefix = "app.datasource.prueba3it")
@ConfigurationProperties(prefix = "spring.datasource")
public class BdPrueba3ItProperties {
	private String url;
	private String username;
	private String password;
	private String driverClassName;
	private String hibernateDialect;
	private String hibernateHbm2ddlAuto;
}
