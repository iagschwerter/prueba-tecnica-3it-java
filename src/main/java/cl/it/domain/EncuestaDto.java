package cl.it.domain;

import lombok.Data;

@Data
public class EncuestaDto {

	private String email;
	private int genero;
	private String nombreGenero;
}
