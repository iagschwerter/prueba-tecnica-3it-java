package cl.it.application;

import org.springframework.stereotype.Service;

import cl.it.adapter.bd.prueba3it.entity.EncuestaMusicaEntity;
import cl.it.adapter.bd.prueba3it.entity.GeneroMusicalEntity;
import cl.it.adapter.bd.prueba3it.service.EncuestaMusicaService;
import cl.it.adapter.bd.prueba3it.service.GeneroMusicalService;
import cl.it.commons.http.Response;
import cl.it.commons.http.Status;
import cl.it.domain.EncuestaDto;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class GuardaEncuestaService {
	
	
	private final GeneroMusicalService generoMusicalService;
	private final EncuestaMusicaService encuestaMusicaService;
	public Response<Void> guardaEncuestaMusical(EncuestaDto dto) throws Exception {
		
		if(encuestaMusicaService.existe(dto.getEmail())){
			return new Response<Void>(Status.ERROR, "El correo ya ha sido utilizado");
		}
	
		
		EncuestaMusicaEntity entity = new EncuestaMusicaEntity();
		GeneroMusicalEntity generoEntity = generoMusicalService.findbyId(dto.getGenero());
		entity.setMail(dto.getEmail());
		entity.setGeneroMusical(generoEntity);
		encuestaMusicaService.create(entity);
	
		return new Response<Void>(Status.SUCCESS, "Encuesta Guardada Correctamente");
	
	}
    public static String getHelloWorld(){
        return "hello world";
    }

}
